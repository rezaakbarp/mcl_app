import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import BaseLayout from "../views/BaseLayout.vue";
import Produk from "../views/Produk/Produk.vue";
import KaryawanManage from "../views/Karyawan/Karyawan.vue";
import Karyawan from "../views/Karyawan/DaftarKaryawan.vue";
import Login from "../views/Login.vue";
import Flex from "../views/Flex.vue";


const routes = [
  {
    path: "/",
    name: "login",
    component: Login, 
  }, 
  {
    path: "/flex",
    name: "flex",
    component: Flex, 
  }, 
  {
    path: "/dashboard",
    name: "home",
    component: BaseLayout,
    // children: [
    //   { 
    //     path: '', 
    //     name: 'user', 
    //     component: HomeView 
    //   }
    // ],
  }, 
  {
    path: "/produk",
    name: "produk",
    component: Produk, 
  }, 
  {
    path: "/karyawan/managekaryawan",
    name: "Karyawan Manage",
    component: KaryawanManage, 
  }, 
  {
    path: "/karyawan",
    name: "karyawan",
    component: Karyawan, 
  },
   


];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
